# Movie Collection Application

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.11.

From the root folder install dependencies
`npm install`

Run `ng serve` or `npm start` to launch the application. Navigate to `http://localhost:4200/`

### Application contains the below features:

- Movies Search
    - search movies as user types
        - if there are no results, show the error (too many movies or not found)
        - if there is no error, show total results and list of found movies
        - if there are more than 10 results, then link "show more" at the bottom of the page will add to the list next 10 movies for the same request
        - clicking on a particular movie will redirect user to the selected movie detail page

- Movie detail
	- more details is shown on movie detail page
    - for navigating back user can use browser "Back" button or link "Back to Search" at the top of the page. Menu link "Find Movie" will also go to search page and all previous results will still be there
