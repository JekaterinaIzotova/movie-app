import * as fromSearch from './search.reducer';
import * as fromMovie from './movie.reducer';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const appFeatureKey = 'application';

export interface MoviesState {
  [fromSearch.searchFeatureKey]: fromSearch.SearchState;
  [fromMovie.moviesFeatureKey]: fromMovie.MovieState;
}

export interface AppState {
  [appFeatureKey]: MoviesState;
}

export const ROOT_REDUCERS = {
  [fromSearch.searchFeatureKey]: fromSearch.reducer,
  [fromMovie.moviesFeatureKey]: fromMovie.reducer,
};

export const GET_APP_STATE = createFeatureSelector<AppState, MoviesState>(
  appFeatureKey
);


export const GET_SEARCH_STATE = createSelector(
  GET_APP_STATE,
  (state) => state.search
);
export const GET_SEARCH_TITLE = createSelector(
  GET_SEARCH_STATE,
  fromSearch.getTitle
);
export const GET_SEARCH_PAGE_NR = createSelector(
  GET_SEARCH_STATE,
  fromSearch.getPageNr
);
export const GET_SEARCH_ID = createSelector(
  GET_SEARCH_STATE,
  fromSearch.getMovieID
);


export const GET_MOVIES_STATE = createSelector(
  GET_APP_STATE,
  (state) => state.movies
);
export const GET_MOVIES_ALL = createSelector(
  GET_MOVIES_STATE,
  fromMovie.selectAllMovies
);
export const GET_MOVIES_TOTAL_RESULTS = createSelector(
  GET_MOVIES_STATE,
  fromMovie.selectTotalResults
);
export const GET_MOVIE_DETAILS = createSelector(
  GET_MOVIES_STATE,
  fromMovie.selectMovieDetails
);
