import { Action, createReducer, on } from '@ngrx/store';
import { SearchActions } from '../actions';

export const searchFeatureKey = 'search';

export interface SearchState {
  title: string;
  pageNr: string;
  imdbID: string;
}

const initialState: SearchState = {
  title: '',
  pageNr: '1',
  imdbID: ''
};

const searchReducer = createReducer(
  initialState,
  on(SearchActions.SEARCH_MOVIES_BY_TITLE, (state, { title, pageNr }) => {
    return {
      ...state,
      title,
      pageNr
    };
  }),
  on(SearchActions.SEARCH_MOVIE_BY_ID, (state, { imdbID }) => {
    return {
      ...state,
      imdbID
    };
  })
);

export function reducer(state: SearchState | undefined, action: Action) {
  return searchReducer(state, action);
}

export const getTitle = (state: SearchState) => state.title;
export const getPageNr = (state: SearchState) => state.pageNr;
export const getMovieID = (state: SearchState) => state.imdbID;
