import { Action, createReducer, on } from '@ngrx/store';
import { MovieActions } from '../actions';
import { Movie } from '../models/movie.interface';

export const moviesFeatureKey = 'movies';

export interface MovieState {
  movies: Movie[];
  totalResults: string;
  movie: Movie;
}

const initialState: MovieState = {
  movies: [],
  totalResults: '0',
  movie: null
};

const MoviesReducer = createReducer(
  initialState,
  on(MovieActions.FOUND_MOVIES, (state, { movies, totalResults }) => ({
    ...state,
    movies: state.movies.concat(movies),
    totalResults: totalResults === '0' ? state.totalResults : totalResults
  })),
  on(MovieActions.REMOVE_MOVIES_FROM_LIST, (state) => ({
    ...state,
    movies: [],
    totalResults: '0'
  })),
  on(MovieActions.MOVIE_DETAILS, (state, { movie }) => ({
    ...state,
    movie
  })),
  on(MovieActions.REFRESH_MOVIE_DETAILS, (state) => ({
    ...state,
    movie: null
  }))
);

export function reducer(state: MovieState | undefined, action: Action) {
  return MoviesReducer(state, action);
}

export const selectAllMovies = (state: MovieState) => state.movies;
export const selectTotalResults = (state: MovieState) => state.totalResults;
export const selectMovieDetails = (state: MovieState) => state.movie;
