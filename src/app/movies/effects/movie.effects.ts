import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, Effect } from '@ngrx/effects';
import { EMPTY, Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { MovieService } from '../services/movie.service';
import { Movie, Movies } from '../models/movie.interface';
import { SearchActions, MovieActions } from '../actions';

@Injectable()
export class MovieEffects {

  getAllMovies$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
        ofType(SearchActions.SEARCH_MOVIES_BY_TITLE),
        switchMap(({ title , pageNr }) => {
          if (title === '') {
            return EMPTY;
          }
          return this.movieService.getMovies(title, pageNr)
            .pipe(
              map((movies: Movies) => MovieActions.FOUND_MOVIES({ movies: movies.movies, totalResults: movies.totalResults }))
            );
        })
      )
  );

  getMovieDetails$: Observable<Action> = createEffect(
    () => this.actions$.pipe(
        ofType(SearchActions.SEARCH_MOVIE_BY_ID),
        switchMap(({ imdbID }) => {
          if (imdbID === '') {
            return EMPTY;
          }
          return this.movieService.getMovieDetails(imdbID)
            .pipe(
              map((movie: Movie) => MovieActions.MOVIE_DETAILS({ movie }))
            );
        })
      )
  );

  constructor(
    private movieService: MovieService,
    private actions$: Actions
  ) {}
}
