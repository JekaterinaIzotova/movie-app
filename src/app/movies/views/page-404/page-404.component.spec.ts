import { TestBed, async } from '@angular/core/testing';
import { Page404Component } from './page-404.component';

describe('Page404Component', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        Page404Component
      ],
    }).compileComponents();
  }));

  it('should create the component', () => {
    const fixture = TestBed.createComponent(Page404Component);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
