import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, forkJoin, Subject } from 'rxjs';
import { tap, takeUntil } from 'rxjs/operators';

import { MovieService } from '../../services/movie.service';
import { Movie } from '../../models/movie.interface';
import { SearchActions, MovieActions } from '../../actions';
import * as fromRoot from '../../reducers';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  public titleInput$: Observable<string>;
  public pageNr$: Observable<string>;
  public movies$: Observable<Movie[]>;
  public totalResults$: Observable<string>;
  private unsubscribe$: Subject<void> = new Subject<void>();
  public movieNotFound: boolean;
  public tooManyResults: boolean;
  public smthIsWrong: boolean;
  public showNext: boolean;

  constructor(private store: Store<fromRoot.AppState>,
              private movieService: MovieService) {

    this.titleInput$ = this.store.pipe(select(fromRoot.GET_SEARCH_TITLE));
    this.pageNr$ = this.store.pipe(select(fromRoot.GET_SEARCH_PAGE_NR));
    this.totalResults$ = this.store.pipe(select(fromRoot.GET_MOVIES_TOTAL_RESULTS));
    this.movies$ = this.store.pipe(select(fromRoot.GET_MOVIES_ALL));
  }

  ngOnInit() {
    this.showNext = true;
  }

  onTitleInputChange(titleInput: string) {
    this.showNext = true;

    this.store.dispatch(MovieActions.REMOVE_MOVIES_FROM_LIST());
    this.store.dispatch(SearchActions.SEARCH_MOVIES_BY_TITLE({title: titleInput, pageNr: '1'}));
  }

  checkErrorText() {
    this.movieNotFound = false;
    this.tooManyResults = false;
    this.smthIsWrong = false;
    let errorText = '';

    this.movies$
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(result => {
        errorText = this.movieService.errorText;
        if (result.length === 0) {
          this.showNext = false;
          if (errorText === 'not found') {
            this.movieNotFound = true;
          } else if (errorText === 'too many') {
            this.tooManyResults = true;
          } else if (errorText === 'Oops') {
            this.smthIsWrong = true;
          } else {
            this.showNext = true;
          }
        } else {
          if (errorText !== '') {
            this.showNext = false;
          } else {
            this.showNext = true;
          }
        }
      });
  }

  async showNextPage() {
    let pageNrCurrent: string;
    let titleInput: string;

    await forkJoin([
      this.pageNr$.pipe(
        tap(result => {
          pageNrCurrent = result;
        })
      ),
      this.titleInput$.pipe(
        tap(result => {
          titleInput = result;
        })
      )
    ]).subscribe();

    const pageNrNew = (+pageNrCurrent + 1).toString(10);
    this.store.dispatch(SearchActions.SEARCH_MOVIES_BY_TITLE({title: titleInput, pageNr: pageNrNew}));
  }

  isZero(totalResults: string) {
    const totalResultsNr = parseInt(totalResults, 10);
    if (totalResultsNr > 0) {
      return true;
    }
    return false;
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
