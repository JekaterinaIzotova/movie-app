import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import * as fromRoot from '../../reducers';
import { SearchComponent } from './search.component';
import { AppComponent } from 'src/app/app.component';
import { HeaderComponent } from '../header/header.component';
import { MainComponent } from '../main/main.component';
import { MovieDetailsComponent } from '../movie-details/movie-details.component';
import { Page404Component } from '../page-404/page-404.component';
import { SearchActions } from '../../actions';
import { MovieService } from '../../services/movie.service';
import { MovieServiceMock } from '../../services/movie.service.mock';

describe('SearchComponent', () => {
  let fixture: ComponentFixture<SearchComponent>;
  let store: MockStore;
  let instance: SearchComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent,
        MainComponent,
        SearchComponent,
        MovieDetailsComponent,
        Page404Component
      ],
      providers: [
        provideMockStore({
          selectors: [
            { selector: fromRoot.GET_SEARCH_TITLE, value: '' },
            { selector: fromRoot.GET_SEARCH_PAGE_NR, value: '1' },
            { selector: fromRoot.GET_MOVIES_TOTAL_RESULTS, value: '0' },
            { selector: fromRoot.GET_MOVIES_ALL, value: [] },
          ],
        }),
        { provide: MovieService, useClass: MovieServiceMock }
      ],
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(SearchComponent);
      instance = fixture.componentInstance;
      store = TestBed.inject(MockStore);

      spyOn(store, 'dispatch');
    });

  }));

  it('should compile', () => {
    fixture.detectChanges();
    expect(fixture).toBeTruthy();
  });

  it('should dispatch movies. Search action on title change', () => {
    const $title = 'movie title';
    const $page = '1';
    const action = SearchActions.SEARCH_MOVIES_BY_TITLE({ title: $title, pageNr: $page });

    instance.onTitleInputChange($title);

    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});
