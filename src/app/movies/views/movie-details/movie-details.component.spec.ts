import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import * as fromRoot from '../../reducers';
import { SearchComponent } from '../search/search.component';
import { AppComponent } from 'src/app/app.component';
import { HeaderComponent } from '../header/header.component';
import { MainComponent } from '../main/main.component';
import { MovieDetailsComponent } from './movie-details.component';
import { Page404Component } from '../page-404/page-404.component';
import { SearchActions } from '../../actions';
import { ActivatedRoute } from '@angular/router';

describe('MovieDetailsComponent', () => {
  let fixture: ComponentFixture<MovieDetailsComponent>;
  let store: MockStore;
  let instance: MovieDetailsComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HeaderComponent,
        MainComponent,
        SearchComponent,
        MovieDetailsComponent,
        Page404Component
      ],
      providers: [
        provideMockStore({
          selectors: [
            { selector: fromRoot.GET_MOVIE_DETAILS, value: null}
          ],
        }),
        { provide: ActivatedRoute }
      ],
    }).compileComponents().then(() => {
      fixture = TestBed.createComponent(MovieDetailsComponent);
      instance = fixture.componentInstance;
      store = TestBed.inject(MockStore);

      spyOn(store, 'dispatch');
    });

  }));

  it('should compile', () => {
    fixture.detectChanges();
    expect(fixture).toBeTruthy();
  });

  it('should dispatch a movie details. Search action on init', () => {
    const $ID = '123';
    const action = SearchActions.SEARCH_MOVIE_BY_ID({ imdbID: $ID });

    instance.ngOnInit();

    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});
