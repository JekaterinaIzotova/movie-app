import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { Movie } from '../../models/movie.interface';
import { SearchActions, MovieActions } from '../../actions';
import * as fromRoot from '../../reducers';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit, OnDestroy {

  public movie$: Observable<Movie>;
  private imdbID: string;

  constructor(private actRoute: ActivatedRoute,
              private store: Store<fromRoot.AppState>) {
    this.imdbID = actRoute.snapshot.params.imdbID;
  }

  ngOnInit() {
    this.store.dispatch(SearchActions.SEARCH_MOVIE_BY_ID({imdbID: this.imdbID}));
    this.movie$ = this.store.pipe(select(fromRoot.GET_MOVIE_DETAILS));
  }

  ngOnDestroy() {
    this.store.dispatch(MovieActions.REFRESH_MOVIE_DETAILS());
  }
}
