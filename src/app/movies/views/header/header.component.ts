import {Component, OnInit } from '@angular/core';
import { Router, Event, NavigationStart, NavigationError } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public showMobNav: boolean;
  public navInput: HTMLInputElement;

  constructor(private router: Router) {
    router.events.subscribe( (event: Event) => {

      if (event instanceof NavigationStart) {
        this.showMobNav = false;
        this.navInput.checked = false;
      }

      if (event instanceof NavigationError) {
          router.navigateByUrl('/error');
      }
    });
  }

  ngOnInit() {
    this.showMobNav = false;
    this.navInput = document.getElementById('nav-input') as HTMLInputElement;
    this.navInput.checked = false;
  }

  showHideNav() {
    this.showMobNav = !this.showMobNav;
  }
}
