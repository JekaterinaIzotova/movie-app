export interface Movie {
    imdbID: string;
    title: string;
    year: string;
    type: string;
    poster: string;
    plot?: string;
    imdbRating?: string;
    runtime?: string;
    genre?: string;
}

export interface Movies {
    movies: Movie[];
    totalResults: string;
}
