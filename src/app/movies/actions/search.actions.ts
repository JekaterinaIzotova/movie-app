import { createAction, props } from '@ngrx/store';

export const SEARCH_MOVIES_BY_TITLE = createAction(
  '[Search] Search movies by title',
  props<{ title: string, pageNr: string }>()
);

export const SEARCH_MOVIE_BY_ID = createAction(
  '[Search] Search movie by id',
  props<{ imdbID: string }>()
);
