import * as SearchActions from './search.actions';
import * as MovieActions from './movie.actions';

export {
  SearchActions,
  MovieActions
};
