import { createAction, props } from '@ngrx/store';
import { Movie } from '../models/movie.interface';

export const FOUND_MOVIES = createAction(
  '[Movie] List found movies',
  props<{ movies: Movie[], totalResults: string }>()
);

export const REMOVE_MOVIES_FROM_LIST = createAction(
  '[Movie] Remove all movies from list'
);

export const MOVIE_DETAILS = createAction(
  '[Movie] View movie details',
  props<{ movie: Movie }>()
);

export const REFRESH_MOVIE_DETAILS = createAction(
  '[Movie] Remove movie details'
);
