import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Movies, Movie } from '../models/movie.interface';

@Injectable()
export class MovieService {

  public errorText = '';
  private API_URL = 'http://www.omdbapi.com/?apikey=f79aeba3';

  constructor(private http: HttpClient) {}

  public getMovies(queryTitle: string, pageNr: string): Observable<Movies> {
    return this.http
      .get<any>(`${this.API_URL}&s=${queryTitle}&page=${pageNr}`)
        .pipe(
          map(result => {
            if (result.Response === 'True') {
              const moviesArray = Object.keys(result.Search).map((key) => {
                this.errorText = '';
                return {
                  imdbID: result.Search[key].imdbID,
                  title: result.Search[key].Title,
                  year: result.Search[key].Year,
                  type: result.Search[key].Type,
                  poster: result.Search[key].Poster
                };
              });
              return {
                movies: moviesArray,
                totalResults: result.totalResults
              };
            } else {
              if (result.Error === 'Movie not found!') {
                this.errorText = 'not found';
              } else if (result.Error === 'Too many results.') {
                this.errorText = 'too many';
              } else {
                this.errorText = 'Oops';
              }
              return {movies: [], totalResults: '0'};
            }
          }),
          catchError(err => {
            console.error(err);
            return [];
          })
        );
  }

  public getMovieDetails(queryID: string): Observable<Movie> {
    return this.http
      .get<any>(`${this.API_URL}&i=${queryID}&plot=full`)
        .pipe(
          map(result => {
            if (result.Response === 'True') {
              return {
                      imdbID: result.imdbID,
                      title: result.Title,
                      year: result.Year,
                      type: result.Type,
                      poster: result.Poster,
                      plot: result.Plot,
                      imdbRating: result.imdbRating,
                      runtime: result.Runtime,
                      genre: result.Genre
                    };
            } else {
              return null;
            }
          }),
          catchError(err => {
            console.error(err);
            return [];
          })
        );
  }
}
