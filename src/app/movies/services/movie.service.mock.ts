import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Movies, Movie } from '../models/movie.interface';

@Injectable()
export class MovieServiceMock {

  constructor() {}

  public getMovies(queryTitle: string, pageNr: string): Observable<Movies> {
    return new Observable<Movies>();
  }

  public getMovieDetails(queryID: string): Observable<Movie> {
    return new Observable<Movie>();
  }
}
