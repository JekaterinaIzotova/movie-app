import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './movies/views/header/header.component';
import { MainComponent } from './movies/views/main/main.component';
import { MovieDetailsComponent } from './movies/views/movie-details/movie-details.component';
import { SearchComponent } from './movies/views/search/search.component';
import { Page404Component } from './movies/views/page-404/page-404.component';
import { MovieService } from './movies/services/movie.service';
import { ROOT_REDUCERS } from './movies/reducers/index';
import { MovieEffects } from './movies/effects/movie.effects';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    SearchComponent,
    MovieDetailsComponent,
    Page404Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    StoreModule.forRoot(ROOT_REDUCERS),
    EffectsModule.forRoot([
      MovieEffects
    ]),
    StoreModule.forFeature('application', ROOT_REDUCERS),
    RouterModule.forRoot([])
  ],
  exports: [
    RouterModule
  ],
  providers: [
    MovieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
