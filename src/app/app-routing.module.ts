import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './movies/views/main/main.component';
import { Page404Component } from './movies/views/page-404/page-404.component';
import { SearchComponent } from './movies/views/search/search.component';
import { MovieDetailsComponent } from './movies/views/movie-details/movie-details.component';

export const routes: Routes = [
  { path: 'movies', component: MainComponent},
  { path: 'search', component: SearchComponent},
  { path: 'movie-details/:imdbID', component: MovieDetailsComponent},
  { path: '', redirectTo: 'movies', pathMatch: 'full' },
  { path: '**', component: Page404Component, data: { title: 'Not found' }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
